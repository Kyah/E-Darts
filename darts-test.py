import usb.core
import usb.util
import sys

VENDOR_ID = 0x046e
PRODUCT_ID = 0xd300

SIMPLE_VALUES_DICT = { 129:1,33:1,
                       130:2,34:2,
                       131:3,35:3,
                       132:4,36:4,
                       133:5,37:5,
                       134:6,38:6,
                       135:7,39:7,
                       136:8,40:8,
                       137:9,41:9,
                       138:10,42:10,
                       139:11,43:11,
                       140:12,44:12,
                       141:13,45:13,
                       142:14,46:14,
                       143:15,47:15,
                       144:16,48:16,
                       145:17,49:17,
                       146:18,50:18,
                       147:19,51:19,
                       148:20,52:20,
                       57:25
                       }

DOUBLE_VALUES_DICT = { 65:1,66:2,67:3,68:4,69:5,
                       70:6,71:7,72:8,73:9,74:10,
                       75:11,76:12,77:13,78:14,79:15,
                       80:16,81:17,82:18,83:19,84:20,
                       89:25
                       }

TRIPLE_VALUES_DICT = { 97:1,98:2,99:3,100:4,101:5,
                       102:6,103:7,104:8,105:9,106:10,
                       107:11,108:12,109:13,110:14,111:15,
                       112:16,113:17,114:18,115:19,116:20
                        }

BUTTONS_DICT = { 1 : 'Next',
                 3 : 'Miss',
                 4 : 'Out'
                }

cible = usb.core.find(idVendor=VENDOR_ID, idProduct=PRODUCT_ID)

# was it found?
if cible is None:
    raise ValueError('Device not found')
else:
    print 'Device was found'


try:
    cible.set_configuration()
except usb.core.USBError as e:
    sys.exit("Could not set configuration: %s" % str(e))

for cfg in cible:
    for intf in cfg:
        for ep in intf:
            endpoint = ep

print "Please play..."

while 1:
    try:
        data = cible.read(endpoint.bEndpointAddress, endpoint.wMaxPacketSize)
        #print data
        #print data[1]
        if data[1] in SIMPLE_VALUES_DICT :
            print "Simple "+str(SIMPLE_VALUES_DICT[data[1]])
        elif data[1] in DOUBLE_VALUES_DICT :
            print "Double "+str(DOUBLE_VALUES_DICT[data[1]])
        elif data[1] in TRIPLE_VALUES_DICT :
            print "Triple "+str(TRIPLE_VALUES_DICT[data[1]])
        elif data[1] in BUTTONS_DICT :
            print "Button "+str(BUTTONS_DICT[data[1]])
        elif data[1] == 0:
            continue

    except usb.core.USBError as e:
        continue
